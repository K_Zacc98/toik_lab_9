package com.demo.springboot;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieListDto;
import org.springframework.stereotype.Service;

@Service
public interface MovieListInterface {
    MovieListDto readSortedMovies();

    boolean createMovie(CreateMovieDto newMovie);

    boolean deleteMovie(int id);

    boolean updateMovie(CreateMovieDto updatedMovie, int id);
}
