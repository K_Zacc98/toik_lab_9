package com.demo.springboot.rest;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.MovieListInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class MovieApiController {
    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);

    @Autowired
    private MovieListInterface movieListInterface;

    @GetMapping(value = "/movies")
    public ResponseEntity<MovieListDto> getMovies() {
        return new ResponseEntity<>(movieListInterface.readSortedMovies(), HttpStatus.OK);
    }

    @PutMapping(value = "/movies/{id}")
    public ResponseEntity<Void> updateMovie(@PathVariable int id, @RequestBody CreateMovieDto updatedMovieDto) {

        LOG.info("--- id: {}", id);
        LOG.info("--- title: {}", updatedMovieDto.getTitle());
        if (movieListInterface.updateMovie(updatedMovieDto, id)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @PostMapping(value = "/movies")
    public ResponseEntity<Void> createMovie(@RequestBody CreateMovieDto newMovie) {

        if (movieListInterface.createMovie(newMovie)) {
            return new ResponseEntity<>(HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }


    }

    @DeleteMapping(value = "/movies/{id}")
    public ResponseEntity<Void> deleteMovie(@PathVariable int id) {

        LOG.info("--- id: {}", id);
        if (movieListInterface.deleteMovie(id)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
